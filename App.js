import React, { Component } from 'react';
import './App.css';

class App extends Component {
  constructor() {
    super();

    this.state = {
      welcomeMess: null,
    }

    this.getMess = this.getMess.bind(this);
  }

  getMess(user) {
    this.setState({
      welcomeMess: "Xin chào" & user
    })
  }

  render() {
    return (
      <div className="App">
        <Header title="Bạn chưa Login" 
          getMessForHeader={this.getMess}  />
        <Content 
          setSelectStudent={this.setSelectStudent}
          selectStudent={this.state.selectStudent}
          students={this.state.students} />
      </div>
    );
  }
}

class Header extends Component {
  constructor() {
    super();
    this.state = {
      khoaHoc: null
    }

    this.LoginClick = this.LoginClick.bind(this);
    this.ClearLoginClick = this.ClearLoginClick.bind(this);
  }

  LoginClick() {
    this.props.getMessForHeader();
  }

  ClearLoginClick() {
    this.props.getMessForHeader();
  }

  render() {
    return (
      <div className='App-header'>
        {this.props.selectStudent} 
        {this.props.title} {this.state.khoaHoc}
        <input type="username" id="inputUsername" className="form-control" placeholder="Tên đăng nhập" required autofocus />     
        <input type="password" id="inputPassword" className="form-control" placeholder="Mật khẩu" required />
        <button className='button' onClick={this.LoginClick}>Đăng nhập</button>
        <button className='button' onClick={this.ClearLoginClick}>Hủy</button>
      </div>
    )
  }
}


class Content extends Component {
  render() {
    return (
      <div className='App-content'>
        <ol>
          {this.props.students && this.props.students.map((student) => {
            return <Student setSelectStudent={this.props.setSelectStudent}
              selectStudent={this.props.selectStudent}
              key={student.name} name={student.name} team={student.team} />
          })}
        </ol>
      </div>
    )
  }
}

// tao component Student co field la name, team
class Student extends Component {

  handleSelectStudent(student) {
    console.log("the student to select", student);
    this.props.setSelectStudent(student);
  }

  render() {
    return (
      <li>
        {this.props.name} - {this.props.team}
        {this.props.selectStudent}
        <button className='button' onClick={this.handleSelectStudent.bind(this, this.props.name)}>Select</button>
      </li>
    )
  }
}


export default App;
