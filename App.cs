body {
 background-color: #282c34;
 font-size: 17px;
}

.App {
 text-align: center;
 color: #fff;
 width: 500px;
 margin: 0 auto;
}

header {
 padding: 20px;
 border: 1px solid #ccc;
}

input {
   padding: 10px;
   border-radius: 3px;
   border: 1px solid transparent;
}
input:focus {
   outline: none;
}

button {
 padding: 5px 10px;
 border-radius: 5px;
 background: #f39200;
 color: #fff;
 border: 1px solid transparent;
 font-size: 15px;
 margin: 10px;
}
button:hover {
 cursor: pointer;
 opacity: 0.8;
}

